﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Zadanie 1D
namespace ConsoleApp6
{
    class Class2
    {
        private static float x = 0;
        private static float y = 0;
        public static void zad1d()
        {
            try
            {
                Console.WriteLine("Podaj x:");
                x = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Podaj y:");
                y = Convert.ToInt32(Console.ReadLine());
            } catch (Exception)
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Musisz podac liczbe!");
                    System.Threading.Thread.Sleep(200);
                    Console.Clear();

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Musisz podac liczbe!");
                    System.Threading.Thread.Sleep(200);
                    Console.Clear();

                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("Musisz podac liczbe!");
                    System.Threading.Thread.Sleep(200);
                    Console.Clear();

                }
               
            }
            if(y == 0.5 * Math.Exp(x) - 10 * x + 1)
            {
                Console.WriteLine("Punkt o wspolrzednych x={0}, y={1} nalezy do wykresu funkcji", x, y);
            } else
            {
                Console.WriteLine("Punkt o wspolrzednych x={0}, y={1} nie nalezy do wykresu funkcji", x, y);

            }
            Console.ReadKey();
        }
    }
}
